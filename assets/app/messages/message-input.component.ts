import { Component, OnInit } from '@angular/core';

import { Message } from './message';
import { MessageService } from './message.service';
import { ErrorService } from '../errors/error.service';

@Component({
  selector: 'my-message-input',
  template: `
    <section class="col-md-8 col-md-offset-2">
      <form (ngSubmit)="onSubmit(f.value)" #f="ngForm">
        <div class="form-group">
          <label for="content">Content</label>
          <input ngControl="content" type="text" class="form-control" id="content" #input [ngModel]="message?.content">
        </div>
        <button type="submit" class="btn btn-primary">{{ !message ? 'Send message' : 'Save message'}}</button>
        <button type="button" class="btn btn-danger"(click)="onCancel()" *ngIf="message">Cancel</button>
      </form>
    </section>
  `
})
export class MessageInputComponent implements OnInit {
  message: Message = null;
  constructor(private messageService: MessageService, private errorService: ErrorService) {  }

  ngOnInit() {
    this.messageService.messageIsEdit.subscribe(
      message => {
        this.message = message;
      }
    );
  }

  onCancel() {
    this.message = null;
  }

  onSubmit(form:any) {
    if (this.message) {
      this.message.content = form.content;
      this.messageService.updateMessage(this.message)
        .subscribe(
          data => console.log(data),
          error => this.errorService.handleError(error)
        );
      this.message = null;
    } else {
      const message = new Message(form.content, null, 'Dummy');
      this.messageService.addMessage(message)
        .subscribe(
          data => {
            console.log(data);
            this.messageService.messages.push(data);
          },
          error => this.errorService.handleError(error)
        );
    }
  }
}
