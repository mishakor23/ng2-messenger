import { Component, OnInit } from '@angular/core';
import { FormBuilder, ControlGroup, Validators } from '@angular/common';
import { Router } from '@angular/router';

import { User } from './user';
import { AuthService } from './auth.service';
import { ErrorService } from '../errors/error.service';

@Component({
  selector: 'my-signin',
  template: `
    <section class="col-md-8 col-md-offser-2">
      <form [ngFormModel]="myForm" (ngSubmit)="onSubmit()" autocomplete="off">
        <div class="form-group">
          <label fro="email">Email</label>
          <input [ngFormControl]="myForm.find('email')" type="email" id="email" class="form-control">
        </div>
        <div class="form-group">
          <label fro="password">Password</label>
          <input [ngFormControl]="myForm.find('password')" type="password" id="firsName" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary" [disabled]="!myForm.valid">Sign In</button>
      </form>
    </section>
  `,
})
export class SigninComponent implements OnInit {
  myForm: ControlGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private errorService: ErrorService
  ) {  }

  onSubmit() {
    const user = new User(this.myForm.value.email, this.myForm.value.password);
    this.authService.signin(user)
        .subscribe(
          data => {
            localStorage.setItem('token', data.token);
            localStorage.setItem('userId', data.userId);
            this.router.navigateByUrl('/');
          },
          error => this.errorService.handleError(error)
        );

  }

  ngOnInit() {
    this.myForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
}
