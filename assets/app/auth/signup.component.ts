import { Component, OnInit } from '@angular/core';
import { FormBuilder, ControlGroup, Validators } from '@angular/common';

import { User } from './user';
import { AuthService } from './auth.service';
import { ErrorService } from '../errors/error.service';

@Component({
  selector: 'my-signup',
  template: `
    <section class="col-md-8 col-md-offser-2">
      <form [ngFormModel]="myForm" (ngSubmit)="onSubmit()" autocomplete="off">
        <div class="form-group">
          <label fro="firstName">First Name</label>
          <input [ngFormControl]="myForm.find('firstName')" type="text" id="firstName" class="form-control">
        </div>
        <div class="form-group">
          <label fro="lastName">Last Name</label>
          <input [ngFormControl]="myForm.find('lastName')" type="text" id="lastName" class="form-control">
        </div>
        <div class="form-group">
          <label fro="email">Email</label>
          <input [ngFormControl]="myForm.find('email')" type="email" id="email" class="form-control">
        </div>
        <div class="form-group">
          <label fro="password">Password</label>
          <input [ngFormControl]="myForm.find('password')" type="password" id="firsName" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary" [disabled]="!myForm.valid">Sign Un</button>
      </form>
    </section>
  `,
})
export class SignupComponent implements OnInit {
  myForm: ControlGroup;

  constructor(private fb: FormBuilder, private authService: AuthService,  private errorService: ErrorService) {  }

  onSubmit() {
    const user = new User(
      this.myForm.value.email,
      this.myForm.value.password,
      this.myForm.value.firstName,
      this.myForm.value.lastName
    );
    this.authService.signup(user)
      .subscribe(
        data => console.log(data),
        error => this.errorService.handleError(error)
      );
  }

  ngOnInit() {
    this.myForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
}
