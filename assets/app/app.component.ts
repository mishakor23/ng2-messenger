import { Component } from '@angular/core';
import { Routes, ROUTER_DIRECTIVES } from '@angular/router';

import { MessagesComponent } from './messages/messages.component';
import { AuthComponent } from './auth/auth.component';
import { HeaderComponent } from './header.component';
import { ErrorComponent } from './errors/error.component';

@Component({
    selector: 'my-app',
    template: `
      <div class="container">
        <my-header></my-header>
        <router-outlet></router-outlet>
      </div>
      <my-error></my-error>
    `,
    directives: [ROUTER_DIRECTIVES, HeaderComponent, ErrorComponent]
})

@Routes([
  {path: '/', component: MessagesComponent},
  {path: '/auth', component: AuthComponent}
])

export class AppComponent {

}
