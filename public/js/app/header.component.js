"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var HeaderComponent = (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () { };
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'my-header',
            template: "\n  <header class=\"row\">\n    <nav class=\"col-md-8 col-md-oofset-2\">\n      <ul class=\"nav nav-pills\">\n        <li><a [routerLink]=\"['/']\">Messenger</a></li>\n        <li><a [routerLink]=\"['/auth']\">Authentication</a></li>\n      </ul>\n    </nav>\n  </header>\n  ",
            directives: [router_1.ROUTER_DIRECTIVES],
            styles: ["\n      header {\n        margin-bottom: 20px;\n      }\n      ul {\n        text-align: center;\n      }\n      li {\n        float: none;\n        display: inline-block;\n      }\n      .router-link-active {\n        background-color: #337ab7;\n        color: white;\n      }\n    "]
        }), 
        __metadata('design:paramtypes', [])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCx1QkFBa0MsaUJBRWxDLENBQUMsQ0FGa0Q7QUFnQ25EO0lBQ0U7SUFBaUIsQ0FBQztJQUVsQixrQ0FBUSxHQUFSLGNBQVksQ0FBQztJQWpDZjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixRQUFRLEVBQUUscVJBU1Q7WUFDRCxVQUFVLEVBQUUsQ0FBQywwQkFBaUIsQ0FBQztZQUMvQixNQUFNLEVBQUUsQ0FBQyw2UkFlTixDQUFDO1NBQ0wsQ0FBQzs7dUJBQUE7SUFLRixzQkFBQztBQUFELENBSkEsQUFJQyxJQUFBO0FBSlksdUJBQWUsa0JBSTNCLENBQUEiLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBST1VURVJfRElSRUNUSVZFUyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbXktaGVhZGVyJyxcbiAgdGVtcGxhdGU6IGBcbiAgPGhlYWRlciBjbGFzcz1cInJvd1wiPlxuICAgIDxuYXYgY2xhc3M9XCJjb2wtbWQtOCBjb2wtbWQtb29mc2V0LTJcIj5cbiAgICAgIDx1bCBjbGFzcz1cIm5hdiBuYXYtcGlsbHNcIj5cbiAgICAgICAgPGxpPjxhIFtyb3V0ZXJMaW5rXT1cIlsnLyddXCI+TWVzc2VuZ2VyPC9hPjwvbGk+XG4gICAgICAgIDxsaT48YSBbcm91dGVyTGlua109XCJbJy9hdXRoJ11cIj5BdXRoZW50aWNhdGlvbjwvYT48L2xpPlxuICAgICAgPC91bD5cbiAgICA8L25hdj5cbiAgPC9oZWFkZXI+XG4gIGAsXG4gIGRpcmVjdGl2ZXM6IFtST1VURVJfRElSRUNUSVZFU10sXG4gIHN0eWxlczogW2BcbiAgICAgIGhlYWRlciB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICB9XG4gICAgICB1bCB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIH1cbiAgICAgIGxpIHtcbiAgICAgICAgZmxvYXQ6IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIH1cbiAgICAgIC5yb3V0ZXItbGluay1hY3RpdmUge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3YWI3O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICB9XG4gICAgYF1cbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgY29uc3RydWN0b3IoKSB7ICB9XG5cbiAgbmdPbkluaXQoKSB7fVxufVxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
