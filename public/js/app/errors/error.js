"use strict";
var Error = (function () {
    function Error(title, message) {
        this.title = title;
        this.message = message;
    }
    return Error;
}());
exports.Error = Error;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9ycy9lcnJvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFDRSxlQUNTLEtBQWEsRUFDYixPQUFlO1FBRGYsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUNiLFlBQU8sR0FBUCxPQUFPLENBQVE7SUFDdEIsQ0FBQztJQUNMLFlBQUM7QUFBRCxDQUxBLEFBS0MsSUFBQTtBQUxhLGFBQUssUUFLbEIsQ0FBQSIsImZpbGUiOiJlcnJvcnMvZXJyb3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgIEVycm9yIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIHRpdGxlOiBzdHJpbmcsXG4gICAgcHVibGljIG1lc3NhZ2U6IHN0cmluZ1xuICApe31cbn1cbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
