"use strict";
var User = (function () {
    function User(email, password, firstName, lastName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return User;
}());
exports.User = User;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImF1dGgvdXNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFDRSxjQUNTLEtBQWEsRUFDYixRQUFnQixFQUNoQixTQUFrQixFQUNsQixRQUFpQjtRQUhqQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixjQUFTLEdBQVQsU0FBUyxDQUFTO1FBQ2xCLGFBQVEsR0FBUixRQUFRLENBQVM7SUFDeEIsQ0FBQztJQUVMLFdBQUM7QUFBRCxDQVJBLEFBUUMsSUFBQTtBQVJZLFlBQUksT0FRaEIsQ0FBQSIsImZpbGUiOiJhdXRoL3VzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVXNlciB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBlbWFpbDogc3RyaW5nLFxuICAgIHB1YmxpYyBwYXNzd29yZDogc3RyaW5nLFxuICAgIHB1YmxpYyBmaXJzdE5hbWU/OiBzdHJpbmcsXG4gICAgcHVibGljIGxhc3ROYW1lPzogc3RyaW5nIFxuICApe31cblxufVxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
